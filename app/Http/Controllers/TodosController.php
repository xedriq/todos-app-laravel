<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;

class TodosController extends Controller
{
    public function index(){
        // fetch all todos from database
        // display them in the to dos index page
        $todos = Todo::all();
        return view('todos.index')->with('todos', $todos);
    }

    public function show(Todo $todo){
        return view('todos.show', ['todo'=>$todo]);
    }

    public function create(){
        return view('todos.create');
    }


    public function store(){
        // validation
        $this->validate(request(), [
            'name' => 'required|min:5',
            'description' => 'required'
        ]);

        $data = request()->all();

        $todo = new Todo;
        $todo->name = $data['name'];
        $todo->description = $data['description'];
        $todo->completed = false;

        $todo->save();
        session()->flash('success', 'A to do is created succesfully.');

        return redirect('/todos');
    }

    public function edit(Todo $todo){
        return view('todos.edit')->with('todo', $todo);
    }

    public function update(Todo $todo){
        $this->validate(request(), [
            'name' => 'required|min:5',
            'description' => 'required'
        ]);

        $data = request()->all();
        $todo->name = $data['name'];
        $todo->description = $data['description'];
        $todo->save();
        session()->flash('success', 'A to do is updated succesfully.');

        return redirect('/todos');
    }

    public function destroy(Todo $todo){
        $todo->delete();
        return redirect('/todos');
    }

    public function complete(Todo $todo){

        $todo->completed = true;
        $todo->save();
        session()->flash('success', 'A to do is updated succesfully.');

        return redirect('/todos');
    }
}
