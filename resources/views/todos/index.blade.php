@extends('layouts.app')

@section('title')
| Index
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-md-8 mx-auto">
        <div class="card my-3">
            <div class="card-header">To Dos</div>
            <div class="card-body">
                <ul class="list-group">
                    @foreach($todos as $todo)
                    <li class="list-group-item">{{ $todo->name }}
                        @if(!$todo->completed)
                        <a href="/todos/{{ $todo->id }}/complete" class="btn btn-warning btn-sm float-right">Complete</a>
                        @endif
                        <a href="/todos/{{ $todo->id }}" class="btn btn-primary btn-sm float-right mr-2">View</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection