@extends('layouts.app')

@section('title')
| {{ $todo->name }}
@endsection

@section('content')
<h1 class="text-center my-4">{{ $todo->name }}</h1>
<div class="row">
    <div class="col-12 col-md-8 mx-auto">
        <div class="card my-4">
            <div class="card-header"> Details </div>
            <div class="card-body">
                <div class="card-text">
                {{ $todo->description }}
                </div>
            </div>
            <div class="card-footer">Completed: {{ $todo->completed }}</div>
        </div>
        <a href="/todos/{{ $todo->id }}/edit" class="btn btn-info">Edit</a>
        <a href="/todos/{{ $todo->id }}/delete" class="btn btn-danger">Delete</a>
    </div>
</div>
@endsection