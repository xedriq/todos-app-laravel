@extends('layouts.app')

@section('title')
| Create new To Do
@endsection

@section('content')
    <h2 class="text-center mt-4">Edit To Do</h2>
    <div class="row">
        <div class="col-12 col-md-8 mx-auto">
        <form action="/todos/{{$todo->id}}/update-todo" method="POST">
                @csrf
                <div class="form-group has-danger">
                    <label class="control-label" for="name">Name: </label>
                    <input 
                        type="text" 
                        id="name" 
                        name="name" 
                        class="form-control
                        @if($errors->has('name'))
                        is-invalid
                        @endif"
                        value="{{ $todo->name }}"
                    >
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="description">Description: </label>
                    <textarea 
                        name="description"
                        type="text" 
                        id="description" 
                        class="form-control
                            @if($errors->has('description'))
                            is-invalid
                            @endif
                        "
                        rows="10"    
                    >{{ $todo->description}}</textarea>
                    <div class="invalid-feedback">{{ $errors->first('description') }}</div>

                </div>

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success">Update To Do</button>
                </div>

            </form>
        </div>
    </div>
@endsection