<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Todo;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'name' => $faker->realText(50,3),
        'description' => $faker->paragraph(3),
        'completed' => false
    ];
});
